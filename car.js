var Car = /** @class */ (function () {
    function Car(make, model, color, speed) {
        if (speed === void 0) { speed = 3; }
        this.make = make;
        this.model = model;
        this.color = color;
        this.speed = speed;
    }
    Car.prototype.accelerate = function () {
        this.speed++;
    };
    Car.prototype.slowDown = function () {
        this.speed--;
    };
    Car.prototype.halt = function () {
        this.speed = 0;
    };
    Object.defineProperty(Car.prototype, "Color", {
        get: function () {
            return this.color;
        },
        set: function (color) {
            this.color = color;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Car.prototype, "Speed", {
        get: function () {
            return this.speed;
        },
        enumerable: true,
        configurable: true
    });
    return Car;
}());
var car = new Car("BMW", 'X-5', 'red', 0);
car.accelerate();
car.Color = 'blue';
console.log(car.Color);
