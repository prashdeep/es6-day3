class Car {

    

    constructor(private make:string, private model:string, 
                private _color?:string, private _speed:number = 3 ){
    }

    accelerate():void{
        this._speed++;
    }

    slowDown():void{
        this._speed--;
    }

    halt():void{
        this._speed = 0;
    }

    get color():string {
        return this._color;
    }

    set color(color:string){
        this._color = color;
    }

    get speed():number{
        return this.speed;
    }

    
}

function printCarDetails(car:Car){
    car.color
}
