function calculateArea(shape:Coordinate){
     return `The area is ${shape.getCoordinates()} `;
}
interface Coordinate{
    getCoordinates():string;
}

interface Interface2{
    printName(firstName:string, lastName:string):string;
}


class CoordinateImpl implements Coordinate, Interface2{

    constructor(private _x:number, private _y:number){
    }

    getCoordinates(){
        return `The coordinates is ${this.x} and ${this.y}`;
    }

    get x(){
        return this._x;
    }

    get y(){
        return this._y;
    }

    printName(firstName:string, lastName:string){
        return `The full name of employee is ${firstName}  ${lastName}`
    }
}

console.log(calculateArea(new CoordinateImpl(34,45)));