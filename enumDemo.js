var Color;
(function (Color) {
    Color[Color["RED"] = 12] = "RED";
    Color[Color["BLUE"] = 24] = "BLUE";
    Color[Color["GREEN"] = 67] = "GREEN";
    Color[Color["ORANGE"] = 44] = "ORANGE";
})(Color || (Color = {}));
console.log("Color is " + Color.GREEN);
function displayFavColor(color) {
    switch (color) {
        case Color.BLUE:
            console.log(color);
            break;
        case Color.GREEN:
            console.log('green');
            break;
    }
}
displayFavColor(Color.BLUE);
